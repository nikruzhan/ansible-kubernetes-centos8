# Ansible playbook to setup K8s on CentOS8/RHEL8

1. Small project to setup single master node for kubernetes(K8s) on Virtualbox VM
2. Two playbooks:
    - prepare_os_firewalld.yml #This will prepare the OS before setup kubernetes
    - setup_k8s_single_master.yml #This will setup kubernetes for single Master node
3. VM requirements (recommended: 2 CPU and 8196MB with 20GB of storage)
4. Setup is using docker as CRI and set native cgroupdriver to systemd
5. Setup is using calico pod network https://docs.projectcalico.org/manifests/calico.yaml
