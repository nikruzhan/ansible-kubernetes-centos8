---
 - name: Configure kubernetes with docker on k8s Master node
   hosts: localhost
   become: yes
   tasks:
   - name: Check hostname
     command: hostname -s
     register: my_hostname
     changed_when: no

   - name: Remove hostname if already exist in /etc/hosts
     lineinfile:
       regexp: "^(.*){{ my_hostname.stdout }}"
       state: absent
       path: /etc/hosts

   - name: Added hostname into /etc/hosts
     shell:
       cmd: echo "`nmcli -g ip4.address connection show enp0s3 | sed -e 's/\(.*\).../\1/'`   `hostname -s`" >> /etc/hosts

   - name: Setup repository for Docker
     yum_repository:
        name: docker-ce
        description: Docker CE repo
        baseurl: https://download.docker.com/linux/centos/8/x86_64/stable/
        enabled: yes
        gpgcheck: yes
        gpgkey: https://download.docker.com/linux/centos/gpg

   - name: Uninstall podman, runc and buildah
     dnf:
      name: "{{ item }}"
      state: absent
      autoremove: yes
     loop:
        - podman
        - runc
        - buildah

   - name: Install docker-ce
     dnf:
       name: docker-ce
       state: present

   - name: Start and enable docker
     systemd:
        name: docker
        state: started
        enabled: yes

   - name: Add vagrant user to docker group
     user:
        name: vagrant
        group: docker

   - name: Remove swapfile from /etc/fstab
     mount:
       name: "{{ item }}"
       fstype: swap
       state: absent
     with_items:
       - swap
       - none

   - name: Disable swap
     command: swapoff -a
     when: ansible_swaptotal_mb > 0

   - name: Install curl
     dnf:
       name: curl
       state: present

   - name: Install docker-compose
     get_url:
       url: https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64
       dest: /usr/local/bin/docker-compose
       mode: '0755'

   - name: Setup repo for Kubernetes
     yum_repository:
        name: kubernetes
        description: Kubernetes repo
        baseurl: https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64/
        enabled: yes
        gpgcheck: yes
        gpgkey:
          - https://packages.cloud.google.com/yum/doc/yum-key.gpg
          - https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

   - name: Install kubernetes
     dnf:
       name: "{{ item }}"
       state: present
     loop:
        - kubeadm
        - kubectl
        - kubelet

   - name: Firewalld - open necessary ports used by Kubernetes
     firewalld:
       port: "{{ item }}"
       permanent: yes
       state: enabled
     loop:
         - 443/tcp
         - 6443/tcp
         - 2379-2380/tcp
         - 10250-1252/tcp
         - 10255/tcp #required?
         - 4243/tcp

   - name: Firewalld - Add docker0 interface to Trusted zone
     firewalld:
       zone: trusted
       state: enabled
       interface: docker0
       permanent: yes
       #port: 4243/tcp

   - name: Firewalld - Add masquerade to Public zone
     firewalld:
       zone: public
       state: enabled
       permanent: yes
       masquerade: yes

   - name: Reload firewalld
     systemd:
       name: firewalld
       state: reloaded

   - name: Add br_netfilter kernel module
     command: modprobe br_netfilter

   - name: Enable bridge-nf-call-iptables
     command: echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

   - name: Create /etc/sysctl.d/k8s.conf
     copy:
       content: |
                 net.bridge.bridge-nf-call-ip6tables = 1
                 net.bridge.bridge-nf-call-iptables = 1
                 net.ipv4.ip_forward = 1
       dest: /etc/sysctl.d/k8s.conf

   - name: Reload sysctl
     command: sysctl --system

